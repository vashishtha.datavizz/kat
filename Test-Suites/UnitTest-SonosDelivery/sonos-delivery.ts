<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>sonos-delivery</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>639a0f8b-9b95-4445-a1e6-c37c892de868</testSuiteGuid>
   <testCaseLink>
      <guid>c131f15e-6e2e-4eaf-a2f0-c8ed5ed41ddb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Category</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a625023-a65b-4455-9eec-5ba54c3a3716</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/coupon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6ec7a36-f287-4ba9-94cc-f760fd25270e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Customer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c5b661b-e9c8-4769-b0ce-ae353ac884b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ff18e26-b986-4dba-9287-f9b2d32c6626</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Itemupdate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1648b5b-6a4c-4bf6-b1a9-9206403b5a0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>976e94b8-676b-4f17-83ca-669d899526c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/OrderCount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82f379ca-5679-4151-88f2-24bbad3da3cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/OrderDetails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d3d7bcf-d7c0-4f9f-ae8f-4a211c098d46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/OrderInvoice</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2160748a-92f9-4db2-bf26-2b8d6ede120b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/OrderStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c08b56bd-415b-4b0d-8953-57e62f04698d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/OrderType</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f428e578-fa29-46de-8ac4-f63b2a3e1f91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40cb4b03-a933-492e-9f29-43b509103939</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/paymentdetail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>247d1989-6bfe-4536-8479-d14e389d239b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/PaymentDetailMaster</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b029302-270c-4136-82ec-537ea5e0d175</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/paytmsecond</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c02b693-724b-4d6f-a2d4-c86f486744eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/radetail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51879a63-2e5e-43e2-ba19-b543bcc356a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/rdetail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa56fea-71f2-488b-89d3-bd22ee78a206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Restaurant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcea269f-5fdc-472b-a141-9f3b8ff92e53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/RestaurantAddress</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f943a77-8b54-4335-86a2-4167ae8596d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/RestaurantTime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af34976d-88e1-4220-9534-90d17dfbade1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Special</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f565eef-0a29-4b21-8ba8-45bcaf2fe57f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Stuffing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f417129c-44d1-4397-85a9-9f86ebc2196a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REST examples/Sonos-Delivery-Load Testing/Type</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
