<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>radetail-post</name>
   <tag></tag>
   <elementGuidId>f66f098a-48dc-4d41-beaf-37548eb2f582</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\n\&quot;Fullname\&quot;:\&quot;KBOBS\&quot;,\n\n\&quot;Email\&quot;:\&quot;dhruvishah0403@gmail.com\&quot;,\n\n\&quot;Password\&quot;:\&quot;101\&quot;,\n\n\&quot;Contact_Number\&quot;:\&quot;9601705467\&quot;,\n\n\&quot;Branch_Name\&quot;:\&quot;naroda\&quot;,\n\n\&quot;Branch_Location\&quot;:\&quot;1, maruti complex,G.F. Nr. gangotri hotel,sutariya karkhana,NH-8, naroda \&quot;,\n\n\&quot;Latitude\&quot;:\&quot;23.05591900\&quot;,\n\n\&quot;Longitude\&quot;:\&quot;72.63264400\&quot;,\n\n\&quot;EDT\&quot;:\&quot;07:00:20\&quot;,\n\n\&quot;Delivery_Radius\&quot;:3,\n\n\&quot;Delivery_Charge\&quot;:20,\n\n\&quot;Delivery_Option\&quot;:3,\n\n\&quot;Payment_Option\&quot;:1,\n\n\&quot;Restaurant_AddressId\&quot;:4\n\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://phei8cpx11.execute-api.us-east-1.amazonaws.com/Prod/radetail</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
