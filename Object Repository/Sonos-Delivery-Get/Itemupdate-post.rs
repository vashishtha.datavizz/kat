<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Itemupdate-post</name>
   <tag></tag>
   <elementGuidId>a729e2a7-f442-42c4-9a92-48a1a34c2bc8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;Item_Id\&quot;:\&quot;139\&quot;,\n    \&quot;Name\&quot;:\&quot;Chicken Chole\&quot;,\n    \&quot;Category_Id\&quot;:\&quot;6\&quot;,\n    \&quot;Type_Id\&quot;:\&quot;1\&quot;,\n    \&quot;Stuffing_Id\&quot;:\&quot;2\&quot;,\n    \&quot;Cost\&quot;:\&quot;300\&quot;,\n    \&quot;ChefSpecial_Status\&quot;:\&quot;1\&quot;,\n    \&quot;MenuSpecial_Status\&quot;:\&quot;1\&quot;,\n    \&quot;Description\&quot;:\&quot;Anything\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://phei8cpx11.execute-api.us-east-1.amazonaws.com/Prod/itemupdate</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
